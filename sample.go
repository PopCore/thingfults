package thingfults

import (
	"time"
)

// Sample type holds a timestamped value single
// as measured by a Sensor.
type Sample struct {
	Value     string    `json:"value" db:"value"`
	Timestamp time.Time `json:"timestamp" db:"timestamp"`
	Latitude  float64   `json:"lat" db:"lat"`
	Longitude float64   `json:"long" db:"long"`
}
