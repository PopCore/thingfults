package thingfults

import (
	"github.com/thingful/thingfulx"
)

// TsWriter describes a set of functions for writing
// and deleting Samples from a data storage.
type TsWriter interface {
	// SaveSample saves or updates a slices of Thing.Channels to
	// a data storage.
	SaveSamples(thing thingfulx.Thing, thingID int) error

	// DeleteSample removes an individual Sample from the underlying
	// data storage.
	DeleteSample(uid string) error
}
